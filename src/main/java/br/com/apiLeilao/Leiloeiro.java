package br.com.apiLeilao;

import java.util.List;

public class Leiloeiro {

    private String nome;
    private Leilao leilao;


    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance(){
        List<Lance> lances = leilao.getListaDeLances();

        if(lances.isEmpty()){
            throw new RuntimeException("Não existem lances");
        }

        Lance lance  = lances.get(0);

        for(Lance lanceMaior : lances){
            if (lance.getValorDoLance() < lanceMaior.getValorDoLance()){
                lance = lanceMaior;
            }
        }

        return lance;
    }
}
