package br.com.apiLeilao;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTest {

    private Usuario usuario;
    private Lance lance1;
    private Lance lance2;
    private Leilao leilao;
    private Leiloeiro leiloeiro;

    @BeforeEach
    public void inicializa(){
        usuario = new Usuario(1,"Vinicius");
        lance1  = new Lance(usuario, 100.50);
        lance2 = new Lance(usuario, 99.50);
        List<Lance> lstLances = new ArrayList<>();
        lstLances.add(lance1);
        lstLances.add(lance2);

        leilao = new Leilao();
        leiloeiro = new Leiloeiro("José", leilao);
    }

    @Test
    public void testarRetornaMaiorLnace(){
        Lance lance = leiloeiro.retornarMaiorLance();

        Assertions.assertSame(lance1, lance);
    }

    @Test
    public void testarRetornoMaiorLanceListaVazia(){
        List<Lance> lstLances = new ArrayList<>();
        leilao.setListaDeLances(lstLances);
        leiloeiro.setLeilao(leilao);

        Assertions.assertThrows(RuntimeException.class, () -> {leiloeiro.retornarMaiorLance();});
    }
}
