package br.com.apiLeilao;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;
import java.util.List;

public class LeilaoTest {

    private Leilao leilao;
    private Usuario usuario;
    private Lance lance1;

    @BeforeEach
    public void inicializa(){
        usuario = new Usuario(1,"Vinicius");
        lance1  = new Lance(usuario, 100);

        List<Lance> lstLances = new ArrayList<>();
        lstLances.add(lance1);

        leilao = new Leilao();
    }

    @Test
    public void testarValidacaoDeLanceMaior(){
        Lance lance = new Lance(usuario,105);
        Lance respostaLance = leilao.validarLance(lance);

        Assertions.assertSame(lance, respostaLance);
    }

    @Test
    public void testarValidacaoDeLanceMenor(){
        Lance lance = new Lance(usuario,50);
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.incluirNovoLance(lance);});
    }


    @Test
    public void testarInclusaoDeNovoLance(){

        Lance lance = new Lance(usuario,101);
        Lance respostaLance = leilao.incluirNovoLance(lance);

        Assertions.assertSame(lance, respostaLance);
        Assertions.assertEquals(true, leilao.getListaDeLances().contains(lance));
    }

    @Test
    public void testarInclusaoDeNovoLancesErro(){
        Lance lance = new Lance(usuario,90);

        Assertions.assertThrows(RuntimeException.class, () -> {leilao.incluirNovoLance(lance);});
        Assertions.assertEquals(true, leilao.getListaDeLances().contains(lance));
    }
}
